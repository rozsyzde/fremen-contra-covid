# Terms of Use for the Mobile Application FreMEn Explorer

![CTU logo](https://gitlab.fel.cvut.cz/rozsyzde/fremen-contra-covid/-/raw/master/cvut_logo.jpg)

This summary is not a legal document. It is simply a handy reference for understanding the full terms. The software is provided “as is” by **laboratory of chronorobotics of CTU**, without warranty of any kind, express or implied, including but not limited to the warranties of merchantability, fitness for a particular purpose and noninfringement. In no event shall the authors or copyright holders be liable for any claim, damages or other liability, whether in an action of contract, tort or otherwise, arising from, out of or in connection with the software or the use or other dealings in the software.  
This software can be used by individuals only, use by government or companies is allowed only with written consent of the developers. You may not modify, copy, distribute, reproduce, publish, license or sell without the written consent of the developers.  
By downloading and using the app, the user agrees to the terms of use and acceptable use policy.  

**Disclaimer:**  
This software provides only forecasts for occupancy of selected places. The actual occupancy and occurrence of people at places may differ from these forecasts as they are based on past statistical data. This software does not provide or substitute in any way medical or health advice or consultation.  

**Notification on Data Collection Through the App:**  
Data produced and collected by this software is and will remain strictly anonymous and will be open only for research, academic, educational, and or non-profit purposes.  
The app does use third party services that may collect information about you. Link to privacy policy of third party service providers used by the app:  

[Mapbox](https://www.mapbox.com/legal/privacy)  

**Acceptable use policy:**  
This acceptable use policy ("Acceptable Use Policy", "AUP", "Policy") is an agreement between the Mobile Application Developer ("Mobile Application Developer", "us", "we" or "our") and you ("User", "you" or "your"). This Policy sets forth the general guidelines and acceptable and prohibited uses of the “FreMEn Explorer” mobile application and any of its products or services (collectively, "Mobile Application" or "Services").  

**Prohibited activities and uses**  
It is prohibited to use the Services to engage in activities that are illegal under applicable law, that are harmful to others, or that would subject us to liability, including activities connected to any of the following ones, all of which are prohibited under this Policy:  
- Disclosing sensitive personal information about others.  
- Collecting, or attempting to collect, personal information about third parties without their knowledge or consent.  
- Threatening harm to persons or property or other harassing behaviour.  
- Infringing the intellectual property or other proprietary rights of others.  
- Facilitating, aiding, or encouraging any of the above activities through our Services.  

**System abuse**  
Any User in violation of this Policy will be subject to criminal and civil liability. Examples include, but are not limited to, the following:  
- Using or distributing tools designed for compromising the security of the Services.  
- Intentionally or negligently transmitting files containing a computer virus or corrupted data.  
- Accessing another network without permission, including to probe or scan for vulnerabilities, or to breach security or authentication measures.  
- Unauthorized scanning or monitoring of data on any network or system without proper authorization of the owner of the system or network.  

**Service resources**  
It is prohibited to overload the Services or use them in any way that would result in performance issues, or which would interrupt the Services’ accessibility for other Users. Prohibited activities contributing to overloading the Services include (but are not limited to):  
- Deliberate attempts to overload the Services, and broadcast attacks (i.e. denial of service attacks).  
- Engaging in any other activities that diminish the usability and performance of our Services.  

**Enforcement**  
We reserve our right to be the sole arbiter in determining the seriousness of each infringement and to immediately take corrective actions, including the following ones (but not limited to them):  
- Disabling or removal of any content prohibited by this Policy, including cases when we, in our sole discretion, deem it necessary to use this method to prevent harm to others, to us, or to our Services.  
- Reporting violations to law enforcement as determined by us at our sole discretion.  
- Suspension or Termination of Usage of the Services  
Nothing contained in this Policy shall be construed to limit our actions or remedies in any way with respect to any of the prohibited activities. In addition, we reserve at all times all rights and remedies available to us with respect to such activities at law or in equity.  

**Reporting violations**  
If you have encountered a violation of this Policy and would like to report it, please contact us immediately. We will investigate the situation and provide you with full assistance.  

**Changes and amendments**  
We reserve the right to modify this Policy relating to the Mobile Application or the Services at any time, and any such modifications will come into effect upon the publication of an updated version of this Policy. Your continued use of the Mobile Application after any such changes shall constitute your consent to such changes. The Policy was created with https://www.WebsitePolicies.com  

**Acceptance of this policy**  
You acknowledge that you have read this Policy and agree to all its terms and conditions. By using the Mobile Application or its Services you agree to be bound by this Policy. If you do not agree to abide by the terms of this Policy, you are not authorized to use or access the Mobile Application and its Services.  

**Contacting us**  
If you would like to contact us to understand more about this Policy or wish to contact us concerning any matter relating to it, you may do so via the contact form This document was last updated on April 12, 2020.
